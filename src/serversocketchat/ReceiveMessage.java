package serversocketchat;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextArea;

/**
 *
 * @author charlie
 */
public class ReceiveMessage extends Thread{
    String message ="";
    DataInputStream dataIn = null;
    JTextArea strArea = null;
    
    public ReceiveMessage(DataInputStream dataIn, JTextArea strArea) {
        this.dataIn = dataIn;
        this.strArea = strArea;
    }
    
    public void run() {
        while(true) {
            try {
                this.message = this.dataIn.readUTF();
                this.strArea.append("\n" + this.getName()+":"+this.message);
            } catch(IOException ex){
                Logger.getLogger(ReceiveMessage.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
